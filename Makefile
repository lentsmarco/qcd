CC = gcc
CCARGS = -lm -lpthread

SRCS := $(wildcard *.c)
BINS := $(SRCS:%.c=%)
DEBUGBINS := $(BINS:%=%-debug)
PROFILEBINS := $(BINS:%=%-profile)


compile: ${BINS}

%: %.c
		${CC} $< -o $@  ${CCARGS}
		${CC} -DDEBUG -g $< -o $@-debug  ${CCARGS}
		${CC} -pg $< -o $@-profile  ${CCARGS}

clean:
		rm ${BINS} ${PROFILEBINS} ${DEBUGBINS}
