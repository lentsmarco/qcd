#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "pcg_basic.h"
#include <signal.h>


#define d 3
#define N_t 4
#define N_x 4
#define N_y 4
#define N_z 1
#define thermalisationIterations 50000
#define false 0
#define true 1

const int V_s = N_x * N_y * N_z;
const float invV_s = 3.0 / V_s;


#if DEBUG
int verbose = false;

int windingDebug = false;

int signDebug = false;

int mesonVerbose = 0; // value between 0 (not verbose) and 3 (very verbose)

int baryonVerbose = 0; // value between 0 (not verbose) and 3 (very verbose)

int outputStates = false; // whether the lattice states should be output for analysis
#endif

int writeFlag = false;

int timerAverageOver = 5;

int maxTries = 100; // how often a random site is picked and checked in step 1 of the worms, before we give up

float probabilityEpsilon = 0.0001; // to guarantee that some move is made even if floating point errors accumulate

const float defaultStartingGamma = 0.4;
const float defaultEndingGamma = 1.51;
const float defaultGammaStep = 0.1;
const float defaultStartingMu = 0;
const float defaultEndingMu = 1.5;
const float defaultMuStep = 0.1;
const float defaultMaxMuStep = 0.1;
const float defaultMinMuStep = 0.001;
const int defaultAverageOver = 10;
const int defaultErrorSamples = 1000;
const int defaultMaxAverageOver = -1;
const int defaultDynamicalParameters = false;
const float defaultMaxError = 0.05;
const float defaultMaxErrorDecFrac = 0.5;
const float defaultDesireddn = 0.05;
const float defaultDeltadn = defaultDesireddn / 2;
const int defaultThreadCount = 2;

const int V = N_t * N_x * N_y * N_z;
const float a = 1;
const float a_t = 1;
const float mq = 0; // quark mass
float MDPProb;
const int paramN = 3; // SU(N)




struct latticePoint{
  int n; // meson source
  int m; // baryon source
  int k[4][2]; // meson links
  int b[4][2]; // baryon links
  int memberOfBaryonLoop;
};

struct gammaRunParameters{
  float gamma;
  float startingMu;
  float endingMu;
  float muStep;
  int averageOver;
  int NErrorSamples;
  char* filename;
  int dynamicalParameters;
  float maxError;
  float desireddn;
  float deltadn;
  float maxMuStep;
  float minMuStep;
  int maxAverageOver;
  char* lastOutput;
  char* status;
  int* terminalWidth;
  int* done;
};

void mesonWorm(struct latticePoint lattice[N_t][N_x][N_y][N_z], float gamma, pcg32_random_t* pcg32_state);

int mesonWormStep1(struct latticePoint lattice[N_t][N_x][N_y][N_z],int* t0, int* x0, int* y0, int* z0, int* t, int* x, int* y, int* z, int* newnu, int* newforward, pcg32_random_t* pcg32_state);

float mesonWormStep2weight(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z, int rho, int forward, float gamma);

float mesonWormStep2W_D(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z, float gamma);

void mesonWormStep2(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t, int* x, int* y, int* z, int nu, int forward, int* newrho, int* newrhoforward, float gamma, pcg32_random_t* pcg32_state);

int mesonWormStep3(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t, int* x, int* y, int* z, int rho, int rhoforward, int* newnu, int* newforward, pcg32_random_t* pcg32_state);

void baryonWorm(struct latticePoint lattice[N_t][N_x][N_y][N_z], float gamma, float chemmu, pcg32_random_t* pcg32_state);

int baryonWormStep1(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t0, int* x0, int* y0, int* z0, int* t, int* x, int* y, int* z, int* newmu, int*newmuforward, pcg32_random_t* pcg32_state);

float baryonWormStep2weight(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z, int rho, int forward, float gamma, float chemmu);

float baryonWormStep2W_D(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z, float gamma, float chemmu);

void baryonWormStep2(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t, int* x, int* y, int* z, int lastmu, int lastforward, int* newrho, int*newrhoforward, float gamma, float chemmu, pcg32_random_t* pcg32_state);

void baryonWormStep3(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t, int* x, int* y, int* z, int lastrho, int lastrhoforward, int* newmu, int*newmuforward);

int siteState(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z);

void MDPUpdate(struct latticePoint lattice[N_t][N_x][N_y][N_z], float gamma, pcg32_random_t* pcg32_state);

int monomerNumbers(struct latticePoint lattice[N_t][N_x][N_y][N_z]);

float calcChiralCondensate(struct latticePoint lattice[N_t][N_x][N_y][N_z], int averageOver, float gamma, float mu, pcg32_random_t* pcg32_state);

float calcEnergyDensity(struct latticePoint lattice[N_t][N_x][N_y][N_z], int averageOver, float gamma, float mu, pcg32_random_t* pcg32_state);

void calculateObservables(struct latticePoint lattice[N_t][N_x][N_y][N_z], int averageOver, float gamma, float mu, float* baryonDensity, float* sign, float* chiralCondensate, float* energyDensity, pcg32_random_t* pcg32_state);

int windingNumbers(struct latticePoint lattice[N_t][N_x][N_y][N_z]);

int calcSign(struct latticePoint lattice[N_t][N_x][N_y][N_z]);

void validityCheck(struct latticePoint lattice[N_t][N_x][N_y][N_z]);

int countBaryonicSites(struct latticePoint lattice[N_t][N_x][N_y][N_z]);

void initLattice(struct latticePoint lattice[N_t][N_x][N_y][N_z]);

float randReal(pcg32_random_t* pcg32_state);

unsigned int randT(pcg32_random_t* pcg32_state);

unsigned int randX(pcg32_random_t* pcg32_state);

unsigned int randY(pcg32_random_t* pcg32_state);

unsigned int randZ(pcg32_random_t* pcg32_state);

unsigned int randInt(int a, int b, pcg32_random_t* pcg32_state);

int parity(int t, int x, int y, int z);

void printLatticeState(struct latticePoint lattice[N_t][N_x][N_y][N_z]);

void thermalize(struct latticePoint lattice[N_t][N_x][N_y][N_z], int iterations, float gamma, float mu, char* status, pcg32_random_t* pcg32_state);

void printObservables(float startingGamma, float endingGamma, float gammaStep, float startingMu, float endingMu, float muStep, int averageOver, int NErrorSamples, char* filename, int dynamicalParameters, float maxError, float desireddn, float deltadn, float maxMuStep, float minMuStep, int maxErrorSamples, int threadCount);

void jackknife(float values[], int length, float* avg, float* err, float* autocorr);

float autocorrelation(float values[], int length, float average);

void statisticalAnalysis(float values[], int length, float* avg, float* err);

int msleep(long msec);

void pcg32_srandom_r(pcg32_random_t* rng, uint64_t initstate, uint64_t initseq);

uint32_t pcg32_random_r(pcg32_random_t* rng);

void interruptHandler(){
  printf("\e[?25h\n"); // make cursor blink again
  signal(SIGINT,SIG_DFL); // reregister to original hander
  raise(SIGINT);
}


int main(int argc, char *argv[] ){

  signal(SIGINT, interruptHandler);

  if(mq != 0) MDPProb = 1.0 / 4 / pow(mq,2);

  if(N_t % 2 != 0 || N_x % 2 != 0 ||(d > 2 && N_y % 2 != 0) || (d > 3 && N_z % 2 != 0)){
    fprintf(stderr, "lattice has non-even dimension in some direction -> problem with meson loop initialization\n");
    exit(1);
  }

  if((d <= 2 && N_y != 1) || (d <= 3 && N_z != 1)){
    fprintf(stderr, "lattice extent into dimension not considered not 0\n");
    exit(1);
  }

  float startingGamma = defaultStartingGamma;
  float endingGamma = defaultEndingGamma;
  float gammaStep = defaultGammaStep;
  float startingMu = defaultStartingMu;
  float endingMu = defaultEndingMu;
  float muStep = defaultMuStep;
  int averageOver = defaultAverageOver;
  int NErrorSamples = defaultErrorSamples;
  int dynamicalParameters = defaultDynamicalParameters;
  float maxError = defaultMaxError;
  float maxAverageOver = defaultMaxAverageOver;
  float desireddn = defaultDesireddn;
  float deltadn = defaultDeltadn;
  float maxMuStep = defaultMaxMuStep;
  float minMuStep = defaultMinMuStep;
  char* filename = "";
  int threadCount = defaultThreadCount;


  for(int i = 1; i < argc; i++){
    if(!strcmp(argv[i], "-h")){
      char* helpString =  " -sg sets the starting value for gamma (default=%g)\n"
                          " -eg sets the ending value for gamma (default=%g)\n"
                          " -gs sets the stepsize for gamma (default=%g)\n\n"
                          " -sm sets the starting value for mu (default=%g)\n"
                          " -em sets the ending value for mu (default=%g)\n"
                          " -ms sets the stepsize for mu (default=%g)\n\n"
                          " -dyn enables dynamical precision for -avg and -ms (disabled by default)\n"
                          " -merr sets the max error after which -avg is increased (default=%g)\n"
                          " -maxavg sets the maximum number to which -avg can be increased when errors are high. Infinity if -1 (default=%d)\n"
                          " -desdn sets the desired value for delta n according to which the stepsize for mu is adjusted (default=%g)\n"
                          " -deldn sets the max distance to desired delta n after which the stepsize for mu is adjusted (default=%g)\n"
                          " -minms sets the minimum stepsize of mu (default=%g)\n"
                          " -maxms sets the maximum stepsize of mu (default=%g)\n\n"
                          " -avg sets the count for how many configs are averaged for every sample (default=%d)\n"
                          " -smp sets how many samples are collected and jackknifed for each set of parameters (default=%d)\n\n"
                          " -o sets the output filename (the filename is preceeded by the grid dimensions to avoid mixups)\n\n"
                          #if DEBUG
                          " -v sets the verbose flag\n"
                          " -bv sets the baryonVerbose flag (0-3)\n"
                          " -mv sets the mesonVerbose flag (0-3)\n"
                          " -os sets the outputStates flag\n\n"
                          #endif
                          " -ts sets the amount of threads used (default=%d)";
      printf(helpString, defaultStartingGamma, defaultEndingGamma, defaultGammaStep, defaultStartingMu, defaultEndingMu, defaultMuStep, defaultMaxError, defaultMaxAverageOver, defaultDesireddn, defaultDeltadn, defaultMinMuStep, defaultMaxMuStep, defaultAverageOver, defaultErrorSamples, defaultThreadCount);
      exit(0);
    }
    else if(!strcmp(argv[i],"-sg") && i + 1 < argc){
       startingGamma = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-eg") && i + 1 < argc){
       endingGamma = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-gs") && i + 1 < argc){
       gammaStep = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-sm") && i + 1 < argc){
       startingMu = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-em") && i + 1 < argc){
       endingMu = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-ms") && i + 1 < argc){
       muStep = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-avg") && i + 1 < argc){
       averageOver = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-smp") && i + 1 < argc){
       NErrorSamples = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-o") && i + 1 < argc){
       if(strcmp(argv[i+1], "") == 0){
         fprintf(stderr, "filename cannot be empty!\n See -h for help\n");
         exit(1);
       }
       writeFlag = true;
       filename = argv[i+1];
       i++;
     }
     #if DEBUG
    else if(!strcmp(argv[i],"-v")){
       verbose = true;
     }
    else if(!strcmp(argv[i],"-bv") && i + 1 < argc){
       baryonVerbose = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-mv") && i + 1 < argc){
       mesonVerbose = atof(argv[i+1]);
       i++;
     }
    else if(!strcmp(argv[i],"-os") && i + 1 < argc){
       outputStates = atof(argv[i+1]);
       i++;
     }
     #endif
     else if(!strcmp(argv[i],"-dyn")){
        dynamicalParameters = true;
     }
     else if(!strcmp(argv[i],"-merr") && i + 1 < argc){
        maxError = atof(argv[i+1]);
        i++;
    }
    else if(!strcmp(argv[i],"-maxavg") && i + 1 < argc){
       maxAverageOver = atof(argv[i+1]);
       i++;
   }
    else if(!strcmp(argv[i],"-desdn") && i + 1 < argc){
       desireddn = atof(argv[i+1]);
       i++;
    }
    else if(!strcmp(argv[i],"-deldn") && i + 1 < argc){
       deltadn = atof(argv[i+1]);
       i++;
    }
    else if(!strcmp(argv[i],"-maxms") && i + 1 < argc){
       maxMuStep = atof(argv[i+1]);
       i++;
    }
    else if(!strcmp(argv[i],"-minms") && i + 1 < argc){
       minMuStep = atof(argv[i+1]);
       i++;
    }
    else if(!strcmp(argv[i],"-ts") && i + 1 < argc){
       threadCount = atof(argv[i+1]);
       i++;
    }
    else {
      fprintf(stderr, "argument %s not recognized\naborting!\n", argv[i]);
      exit(1);
    }
  }


  printObservables(startingGamma, endingGamma, gammaStep, startingMu, endingMu, muStep, averageOver, NErrorSamples, filename, dynamicalParameters, maxError, desireddn, deltadn, maxMuStep, minMuStep, maxAverageOver, threadCount);
  //if(verbose) cout << "number of baryonic sites:" << countBaryonicSites(lattice) << "\n";
  // if(verbose) for(int t = 0; t < N_t; t++) cout << "(" << t <<",0,0,0) is " << (lattice[0][0][0][0].memberOfBaryonLoop ? "" : "not ") << "member of a baryon loop\n";

  return 0;
}


void mesonWorm(struct latticePoint lattice[N_t][N_x][N_y][N_z], float gamma, pcg32_random_t* pcg32_state){
  #if DEBUG
  if(mesonVerbose > 0) printf("begin meson worm -> initialize variables\n");
  #endif
  int t0;
  int x0;
  int y0;
  int z0;

  int t;
  int x;
  int y;
  int z;

  int lastnu;
  int lastforward;

  int lastrho;
  int lastrhoforward;
  #if DEBUG
  if(mesonVerbose > 0) printf("meson step1\n");
  #endif

  int step1return = mesonWormStep1(lattice, &t0, &x0, &y0, &z0, &t, &x, &y, &z, &lastnu, &lastforward, pcg32_state);
  int step3return;
  if(step1return != 0){
    #if DEBUG
    if(mesonVerbose > 0) printf("meson step1 not completed");
    #endif
    return;
  }

  #if DEBUG
  if(outputStates) printf("m,%d,%d,%d,%d\n", t0, x0, t, x);
  if(outputStates) printLatticeState(lattice);

  struct latticePoint* currentSite = &lattice[t0][x0][y0][z0];

  if(mesonVerbose > 0) printf("(t0, x0, y0, z0)= (%d,%d,%d,%d)\n", t0, x0, y0, z0);
  if(mesonVerbose > 1) printf("variables at (t0, x0, y0, z0): n=%d k[0][0]=%d k[0][1]=%d k[1][0]=%d k[1][1]=%d k[2][0]=%d k[2][1]=%d k[3][0]=%d k[3][1]=%d\n", currentSite -> n, currentSite -> k[0][0], currentSite -> k[0][1], currentSite -> k[1][0], currentSite -> k[1][1], currentSite -> k[2][0], currentSite -> k[2][1], currentSite -> k[3][0], currentSite -> k[3][1]);
  #endif

  do {
    #if DEBUG
    struct latticePoint* currentSite = &lattice[t][x][y][z];
    if(mesonVerbose > 0) printf("(t, x, y, z)= (%d,%d,%d,%d)\n", t, x, y, z);
    if(mesonVerbose > 1) printf("variables at (t, x, y, z): n=%d k[0][0]=%d k[0][1]=%d k[1][0]=%d k[1][1]=%d k[2][0]=%d k[2][1]=%d k[3][0]=%d k[3][1]=%d\n", currentSite -> n, currentSite -> k[0][0], currentSite -> k[0][1], currentSite -> k[1][0], currentSite -> k[1][1], currentSite -> k[2][0], currentSite -> k[2][1], currentSite -> k[3][0], currentSite -> k[3][1]);

    if(mesonVerbose > 0) printf("meson step2\n");
    #endif

    mesonWormStep2(lattice, &t, &x, &y, &z, lastnu, lastforward, &lastrho, &lastrhoforward, gamma, pcg32_state);

    #if DEBUG
    if(outputStates) printf("m,%d,%d,%d,%d\n", t0, x0, t, x);
    if(outputStates) printLatticeState(lattice);

    currentSite = &lattice[t][x][y][z];
    if(mesonVerbose > 0) printf("(t, x, y, z)= (%d,%d,%d,%d)\n", t, x, y, z);
    if(mesonVerbose > 1) printf("variables at (t, x, y, z): n=%d k[0][0]=%d k[0][1]=%d k[1][0]=%d k[1][1]=%d k[2][0]=%d k[2][1]=%d k[3][0]=%d k[3][1]=%d\n", currentSite -> n, currentSite -> k[0][0], currentSite -> k[0][1], currentSite -> k[1][0], currentSite -> k[1][1], currentSite -> k[2][0], currentSite -> k[2][1], currentSite -> k[3][0], currentSite -> k[3][1]);

    if(mesonVerbose > 0) printf("meson step3\n");
    #endif

    step3return = mesonWormStep3(lattice, &t, &x, &y, &z, lastrho, lastrhoforward, &lastnu, &lastforward, pcg32_state);

    #if DEBUG
    fflush(stdout);
    if(mesonVerbose > 0) printf("(t0, x0, t, x) = (%d, %d, %d, %d)\n", t0, x0, t, x);
    if(outputStates) printf("m,%d,%d,%d,%d\n", t0, x0, t, x);
    if(outputStates) printLatticeState(lattice);
    #endif
  } while(step3return != 0);
}

int mesonWormStep1(struct latticePoint lattice[N_t][N_x][N_y][N_z],int* t0, int* x0, int* y0, int* z0, int* t, int* x, int* y, int* z, int* newnu, int* newforward, pcg32_random_t* pcg32_state){
  #if DEBUG
  if(mesonVerbose > 0) printf("beginning meson step 1\n");
  #endif
  int tries = 0;
  while(true){
    tries++;
    if(tries > maxTries){
      #if DEBUG
      if(mesonVerbose > 0){
        fprintf(stderr, "no sites where we can accept a direction were found in meson step 1\n");
        fprintf(stderr, "(t, x, y, z) = (%d, %d, %d, %d)\n", *t, *x, *y, *z);
      }
      if(mesonVerbose > 1) printLatticeState(lattice);
      #endif
      return 1;
    }
    // choose random site
    int siteTries = 0;
    do {
        *t = randT(pcg32_state);
        *x = randX(pcg32_state);
        *y = randY(pcg32_state);
        *z = randZ(pcg32_state);

        siteTries++;
        if(siteTries > maxTries) return 1;
    } while(lattice[*t][*x][*y][*z].memberOfBaryonLoop); // if memberOfBaryonLoop choose another one

    struct latticePoint* currentSite = &lattice[*t][*x][*y][*z];

    // calculate N -- Fromm 2010 eq. (4.11)
    int N = currentSite -> n;
    for(int nu = 0; nu < d; nu++)
    for(int forward = 0; forward < 2; forward++){
      N += currentSite -> k[nu][forward];
    }

    // adjust new probabilities to the fact that previous nus need to be
    // rejected in order for the current nu to even be tested
    float correctionToProbability= 1;

    for(int nu = 0; nu < d; nu++)
    for(int forward = 0; forward < 2; forward++){

      // calculate probability of acceptance -- Fromm 2010 eq. (4.25)
      float P_nu = currentSite -> k[nu][forward] * 1.0 / N / correctionToProbability;
      #if DEBUG
      if(mesonVerbose > 2) printf("N=%d\n", N);
      if(mesonVerbose > 2) printf("n=%d\n", currentSite -> n);
      if(mesonVerbose > 2) printf("k[%d][%d]=%d\n", nu, forward,  currentSite -> k[nu][forward]);
      if(mesonVerbose > 2) printf("P_%d=%f\n", nu, P_nu);
      #endif
      if(randReal(pcg32_state) < P_nu){
        // update link and monomer variables and move to site y
        // this completes step 1
        #if DEBUG
        if(mesonVerbose > 1) printf("chose direction %s%d n: %d->%d k[%d][%d]: %d->%d\n", (forward ? "+" : "-"), nu, currentSite -> n, currentSite -> n + 1, nu, forward, currentSite -> k[nu][forward], currentSite -> k[nu][forward] - 1);
        #endif
        currentSite -> n += 1;
        currentSite -> k[nu][forward] -= 1;


        *t0 = *t;
        *x0 = *x;
        *y0 = *y;
        *z0 = *z;


        *newnu = nu;
        *newforward = forward;
        int step = forward * 2 - 1;
        if(nu == 0) *t = (*t + step + N_t) % N_t;
        if(nu == 1) *x = (*x + step + N_x) % N_x;
        if(nu == 2) *y = (*y + step + N_y) % N_y;
        if(nu == 3) *z = (*z + step + N_z) % N_z;
        #if DEBUG
        if(mesonVerbose > 0) printf("moving %d in direction %d\n", step, nu);
        if(mesonVerbose > 0) printf("new (t, x , y, z) = (%d, %d, %d, %d)\n", *t, *x, *y, *z);
        #endif
        return 0;
      }
      // update the correction for the next nus
      correctionToProbability *= (1 - P_nu);
    }
    // if no nus were accepted, we move to the next random site
  }
  #if DEBUG
  if(mesonVerbose > 0) printf("meson worm step 1 completed");
  #endif
  return 0;
}


float mesonWormStep2weight(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z, int rho, int forward, float gamma){
  int step = forward * 2 - 1;
  if(rho == 0) t = (t + step + N_t) % N_t;
  if(rho == 1) x = (x + step + N_x) % N_x;
  if(rho == 2) y = (y + step + N_y) % N_y;
  if(rho == 3) z = (z + step + N_z) % N_z;
  return (!lattice[t][x][y][z].memberOfBaryonLoop) ? pow(gamma, 2 * (rho == 0)) : 0;
}

float mesonWormStep2W_D(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z, float gamma){
  float W_D = 0;
  for(int nu = 0; nu < d; nu ++)
  for(int forward = 0; forward < 2; forward++){
    W_D += mesonWormStep2weight(lattice, t, x, y, z, nu, forward, gamma);
  }
  return W_D;
}

void mesonWormStep2(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t, int* x, int* y, int* z, int nu, int forward, int* newrho, int* newrhoforward, float gamma, pcg32_random_t* pcg32_state){
  #if DEBUG
  if(mesonVerbose > 0) printf("begin meson step 2 at (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
  #endif

  struct latticePoint* currentSite = &lattice[*t][*x][*y][*z];


  float correctionToProbability = 1;

  float W_D = mesonWormStep2W_D(lattice, *t, *x, *y, *z, gamma);
  float invW_D = 1.0 / W_D;
  if(W_D == 0){
    fprintf(stderr, "W_D=0\n");
    fprintf(stderr, "(t, x, y, z)=(%d, %d, %d, %d), gamma=%f\n", *t, *x, *y, *z, gamma);
    for(int rho = 0; rho < 2; rho++){
      fprintf(stderr, "%g\n", mesonWormStep2weight(lattice, *t, *x, *y, *z, rho, 0, gamma));
      fprintf(stderr, "%g\n", mesonWormStep2weight(lattice, *t, *x, *y, *z, rho, 1, gamma));
    }
    printLatticeState(lattice);
  }
  float P_rho;
  for(int rho = 0; rho < d; rho++)
  for(int rhoforward = 0; rhoforward < 2; rhoforward++){
    P_rho = mesonWormStep2weight(lattice, *t, *x, *y, *z, rho, rhoforward, gamma) * invW_D / correctionToProbability;
    #if DEBUG
    if(mesonVerbose > 2) printf("P_%s%d=%f\n", (rhoforward ? "+" : "-"), rho, P_rho*correctionToProbability);
    #endif
    if(randReal(pcg32_state) < P_rho){
      #if DEBUG
      if(mesonVerbose > 1) printf("k[%d][%d]->-1\n", nu, 1-forward);
      if(mesonVerbose > 1) printf("k[%d][%d]->+1\n", rho, rhoforward);
      #endif

      currentSite -> k[nu][1-forward] -= 1;
      if(currentSite -> k[nu][1-forward] < 0){
        fprintf(stderr, "something went wrong in meson worm\n");
        printLatticeState(lattice);
        exit(1);
      }
      currentSite -> k[rho][rhoforward] += 1;

      *newrho = rho;
      *newrhoforward = rhoforward;



      int step = rhoforward * 2 - 1;
      #if DEBUG
      if(mesonVerbose > 0) printf("moving %d in direction %d\n", step, rho);
      #endif

      if(rho == 0) *t = (*t + step + N_t) % N_t;
      if(rho == 1) *x = (*x + step + N_x) % N_x;
      if(rho == 2) *y = (*y + step + N_y) % N_y;
      if(rho == 3) *z = (*z + step + N_z) % N_z;

      #if DEBUG
      if(mesonVerbose > 0) printf("new (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
      if(mesonVerbose > 0) printf("meson move 2 complete\n");
      #endif

      return;
    }
    correctionToProbability *= (1 - P_rho);
  }
  fprintf(stderr, "meson step 2 not completed\n");
  fprintf(stderr, "last probability: %f\n", P_rho);
  exit(1);
}

int mesonWormStep3(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t, int* x, int* y, int* z, int rho, int rhoforward, int* newnu, int* newforward, pcg32_random_t* pcg32_state){
  #if DEBUG
  if(mesonVerbose > 0) printf("begin meson step 3 at (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
  #endif

  struct latticePoint* currentSite = &lattice[*t][*x][*y][*z];

  // calculate N -- Fromm 2010 eq. (4.11)
  int N = currentSite -> n;
  for(int nu = 0; nu < d; nu++)
  for(int forward = 0; forward < 2; forward++){
    N += currentSite -> k[nu][forward];
  }

  int kmrho = currentSite -> k[rho][1-rhoforward]; // k_{-rho}

  float correctionToProbability = 1;
  float P_z = currentSite -> n * 1.0 /(N - kmrho);
  #if DEBUG
  if(N - kmrho == 0){
    fprintf(stderr, "N-kmrho=0\n");
    fprintf(stderr, "N=%d, kmrho=%d\n", N, kmrho);
    fprintf(stderr, "memberOfBaryonLoop=%d\n", currentSite -> memberOfBaryonLoop);
    fprintf(stderr, "(t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
    printLatticeState(lattice);
  }
  if(mesonVerbose > 2) printf("P_z=%f\n", P_z);
  if(mesonVerbose > 2) printf("N=%d, kmrho=%d\n", N, kmrho);
  #endif
  if(randReal(pcg32_state) < P_z){
    #if DEBUG
    if(mesonVerbose > 0) printf("meson worm closed\n");
    #endif
    currentSite -> n -= 1;
    currentSite -> k[rho][!rhoforward] += 1;
    return 0;
  }
  correctionToProbability *= (1 - P_z);

  float P_mu;

  for(int mu = 0; mu < d; mu++)
  for(int forward = 0; forward < 2; forward++){
    if(mu == rho && forward != rhoforward) continue;


    P_mu = currentSite -> k[mu][forward] * 1.0 / ((N - kmrho) * correctionToProbability);
    #if DEBUG
    if(mesonVerbose > 2) printf("P_mu=%f\n", P_mu);
    #endif

    if(randReal(pcg32_state) < P_mu){
      #if DEBUG
      if(mesonVerbose > 1) printf("k[%d][%d]-> +1\n", rho, 1-rhoforward);
      if(mesonVerbose > 1) printf("k[%d][%d]-> -1\n", mu, forward);
      #endif

      currentSite -> k[rho][1-rhoforward] += 1;
      currentSite -> k[mu][forward] -= 1;

      *newnu = mu;
      *newforward = forward;

      int step = forward * 2 - 1;
      if(mu == 0) *t = (*t + step + N_t) % N_t;
      if(mu == 1) *x = (*x + step + N_x) % N_x;
      if(mu == 2) *y = (*y + step + N_y) % N_y;
      if(mu == 3) *z = (*z + step + N_z) % N_z;
      #if DEBUG
      if(mesonVerbose > 0) printf("moving %d in direction %d\n", step, mu);
      if(mesonVerbose > 0) printf("new (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
      if(mesonVerbose > 0) printf("meson move 3 complete\n");
      #endif
      return 1;
    }
    correctionToProbability *= (1-P_mu);
  }
  fprintf(stderr, "meson step 3 not completed\n");
  fprintf(stderr, "last probability: %f\n", P_mu);
  exit(1);
}

void baryonWorm(struct latticePoint lattice[N_t][N_x][N_y][N_z], float gamma, float chemmu, pcg32_random_t* pcg32_state){
  int t0;
  int x0;
  int y0;
  int z0;

  int t;
  int x;
  int y;
  int z;

  int lastmu;
  int lastmuforward;
  int lastrho;
  int lastrhoforward;

  int step1return = baryonWormStep1(lattice, &t0, &x0, &y0, &z0, &t, &x, &y, &z, &lastmu, &lastmuforward, pcg32_state);
  if(step1return != 0) {
    #if DEBUG
    if(baryonVerbose > 0) printf("baryon worm step 1 failed. errorcode: %d\n", step1return);
    #endif
    return;
}
  #if DEBUG
  if(outputStates) printf("b,%d,%d,%d,%d\n", t0, x0, t, x);
  if(outputStates) printLatticeState(lattice);
  #endif

  do {
    baryonWormStep2(lattice, &t, &x, &y, &z, lastmu, lastmuforward, &lastrho, &lastrhoforward, gamma, chemmu, pcg32_state);

    #if DEBUG
    if(outputStates) printf("b,%d,%d,%d,%d\n", t0, x0, t, x);
    if(outputStates) printLatticeState(lattice);
    #endif

    baryonWormStep3(lattice, &t, &x, &y, &z, lastrho, lastrhoforward, &lastmu, &lastmuforward);

    #if DEBUG
    if(outputStates) printf("b,%d,%d,%d,%d\n", t0, x0, t, x);
    if(outputStates) printLatticeState(lattice);
    #endif

  } while(t != t0 || x != x0 || y != y0 || z != z0);

}

int baryonWormStep1(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t0, int* x0, int* y0, int* z0, int* t, int* x, int* y, int* z, int* newmu, int*newmuforward, pcg32_random_t* pcg32_state){
  #if DEBUG
  if(baryonVerbose > 0) printf("beginning baryon worm step 1\n");
  #endif
  struct latticePoint* currentSite;
  int mu;
  int forward;
  int saturatedSite = false; // whether we have a link with k=3
  #if DEBUG
  if(baryonVerbose > 0) printf("trying to find suitable lattice site\n");
  #endif
  int tries = 0;
  do {
    tries++;
    if(tries > maxTries) return 1;
    *t = randT(pcg32_state);
    *x = randX(pcg32_state);
    *y = randY(pcg32_state);
    *z = randZ(pcg32_state);
    currentSite = &lattice[*t][*x][*y][*z];

    #if DEBUG
    if(baryonVerbose > 1) printf("memberOfBaryonLoop=%d\n", currentSite -> memberOfBaryonLoop);
    #endif

    for(int nu = 0 ; nu < d; nu++)
    for(int nuforward = 0; nuforward < 2; nuforward++){

      #if DEBUG
      if(baryonVerbose > 1) printf("nu=%d nuforward=%d k[%d][%d]=%d\n", nu, nuforward, nu ,nuforward, currentSite -> k[nu][nuforward]);
      if(baryonVerbose > 1) printf("b[%d][%d]=%d\n", nu, nuforward, currentSite -> b[nu][nuforward]);
      #endif

      if(currentSite -> k[nu][nuforward] == 3){

        #if DEBUG
        if(baryonVerbose > 0) printf("mesonic site found\n");
        #endif

        saturatedSite = true;
        mu = nu;
        forward = nuforward;
        break;
      }
      if(currentSite -> b[nu][nuforward] == -1){

        #if DEBUG
        if(baryonVerbose > 0) printf("baryonic site found\n");
        #endif

        mu = nu;
        forward = nuforward;
        break;
      }
    }
  } while(!(currentSite -> memberOfBaryonLoop || saturatedSite));

  *t0 = *t;
  *x0 = *x;
  *y0 = *y;
  *z0 = *z;

  #if DEBUG
  if(baryonVerbose > 0) printf("(t0, x0, y0, z0)=(%d, %d, %d, %d)\n", *t0, *x0, *y0, *z0);
  #endif

  if(currentSite -> memberOfBaryonLoop){

    #if DEBUG
    if(baryonVerbose > 0) printf("baryon site -> b[%d][%d]: %d->0\n", mu, forward, currentSite -> b[mu][forward]);
    #endif

    currentSite -> b[mu][forward] = 0;
  }

  if(saturatedSite){
    currentSite -> b[mu][forward] = 1;
    currentSite -> memberOfBaryonLoop = true;
    currentSite -> k[mu][forward] = 0;
  }

  currentSite -> m = 1; // place symbolic baryon source

  *newmu = mu;
  *newmuforward = forward;

  int step = forward * 2 - 1;
  if(mu == 0) *t = (*t + step + N_t) % N_t;
  if(mu == 1) *x = (*x + step + N_x) % N_x;
  if(mu == 2) *y = (*y + step + N_y) % N_y;
  if(mu == 3) *z = (*z + step + N_z) % N_z;

  #if DEBUG
  if(baryonVerbose > 0) printf("moving %d in direction %d\n", step, mu);
  if(baryonVerbose > 0) printf("new (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
  if(baryonVerbose > 0) printf("baryon move 1 completed\n");
  #endif

  return 0;
}

float baryonWormStep2weight(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z, int rho, int forward, float gamma, float chemmu){
  int step = forward * 2 - 1;
  if(rho == 0) t = (t + step + N_t) % N_t;
  if(rho == 1) x = (x + step + N_x) % N_x;
  if(rho == 2) y = (y + step + N_y) % N_y;
  if(rho == 3) z = (z + step + N_z) % N_z;

  #if DEBUG
  if(baryonVerbose > 2) printf("(t, x, y, z) = (%d, %d, %d, %d)\n", t, x, y, z);
  #endif

  struct latticePoint* currentSite = & lattice[t][x][y][z];

  int saturatedSite = false;
  for(int nu = 0; nu < d; nu++)
  for(int nuforward = 0; nuforward < 2; nuforward++){
    if(currentSite -> k[nu][nuforward] == 3) saturatedSite = true;

    #if DEBUG
    if(baryonVerbose > 2) printf("k[%d][%d]=%d\n", nu, nuforward, currentSite -> k[nu][nuforward]);
    #endif
  }
  #if DEBUG
  if(baryonVerbose > 3) printf("gamma^3=%f, exp=%f\n", pow(gamma, 3 * (rho == 0)), exp(3 * a_t * chemmu *((rho == 0 && forward == 1) - (rho == 0 && forward == 0))));
  if(baryonVerbose > 2) printf("memberOfBaryonLoop=%d, saturatedSite=%d\n", currentSite -> memberOfBaryonLoop, saturatedSite);
  #endif

  return (currentSite -> memberOfBaryonLoop || saturatedSite) ? pow(gamma, 3 * (rho == 0)) * exp(3 * a_t * chemmu *((rho == 0 && forward == 1) - (rho == 0 && forward == 0))) : 0;
}

float baryonWormStep2W_D(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z, float gamma, float chemmu){
  float sum = 0;
  for(int nu = 0; nu < d; nu++)
  for(int forward = 0; forward < 2; forward++){
    sum += baryonWormStep2weight(lattice, t, x, y, z, nu, forward, gamma, chemmu);
  }
  return sum;
}

void baryonWormStep2(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t, int* x, int* y, int* z, int lastmu, int lastforward, int* newrho, int*newrhoforward, float gamma, float chemmu, pcg32_random_t* pcg32_state){

  #if DEBUG
  if (baryonVerbose > 0) printf("begin baryon step 2 at (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
  #endif

  struct latticePoint* currentSite = & lattice[*t][*x][*y][*z];

  float correctionToProbability = 1;
  float W_D = baryonWormStep2W_D(lattice, *t, *x, *y, *z, gamma, chemmu);
  float invW_D = 1.0 / W_D;

  float P_rho;
  for(int rho = 0; rho < d; rho++)
  for(int forward = 0; forward < 2; forward++){
    float weight = baryonWormStep2weight(lattice, *t, *x, *y, *z, rho, forward, gamma, chemmu);
    P_rho = weight * invW_D / correctionToProbability;

    #if DEBUG
    if(baryonVerbose > 2) printf("weight=%f, W_D=%f\n", weight, W_D);
    if(baryonVerbose > 2) printf("P_%s%d=%f\n", (forward ? "+" : "-"), rho, P_rho);
    #endif

    if(randReal(pcg32_state) < P_rho){
      #if DEBUG
      if(baryonVerbose > 1) printf("chose direction %s%d for next move \nb[%d][%d]: %d->%d\nk[%d][%d]: %d->0\n", (forward ? "+" : "-"), rho, lastmu, !lastforward, currentSite -> b[lastmu][!lastforward], currentSite -> b[lastmu][!lastforward] - 1, lastmu, !lastforward, currentSite -> k[lastmu][!lastforward]);
      #endif
      currentSite -> b[lastmu][!lastforward] -= 1;
      #if DEBUG
      if(baryonVerbose > 1) printf("b[%d][%d]: %d->%d\n", rho, forward, currentSite -> b[rho][forward], currentSite -> b[rho][forward] + 1);
      #endif
      currentSite -> k[lastmu][!lastforward] = 0;
      currentSite -> b[rho][forward] += 1;
      if(currentSite -> b[rho][forward] == 0){
        #if DEBUG
        if(baryonVerbose > 1) printf("erased baryon lines from this site -> k[%d][%d]=3, memberOfBaryonLoop = false\n", rho, forward);
        #endif
        currentSite -> k[rho][forward] = 3;
        currentSite -> memberOfBaryonLoop = false;
      }else{
        currentSite -> memberOfBaryonLoop = true;
      }
      *newrho = rho;
      *newrhoforward = forward;

      int step = forward * 2 - 1;
      if(rho == 0) *t = (*t + step + N_t) % N_t;
      if(rho == 1) *x = (*x + step + N_x) % N_x;
      if(rho == 2) *y = (*y + step + N_y) % N_y;
      if(rho == 3) *z = (*z + step + N_z) % N_z;
      #if DEBUG
      if(baryonVerbose > 0) printf("moving %d in direction %d\n", step, rho);
      if(mesonVerbose > 0) printf("new (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
      if(mesonVerbose > 0) printf("baryon move 2 complete\n");
      #endif
      return;
    }
    correctionToProbability *= (1-P_rho);
  }
  fprintf(stderr, "baryon step 2 not completed\n");
  fprintf(stderr, "last correction: %f\n", correctionToProbability);
  fprintf(stderr, "last probability: %f\n", P_rho);
  exit(1);
}

void baryonWormStep3(struct latticePoint lattice[N_t][N_x][N_y][N_z], int* t, int* x, int* y, int* z, int lastrho, int lastrhoforward, int* newmu, int*newmuforward){
  #if DEBUG
  if (baryonVerbose > 0) printf("begin baryon step 3 at (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
  #endif
  struct latticePoint* currentSite = &lattice[*t][*x][*y][*z];
  if(currentSite -> m == 1){
    #if DEBUG
    if(baryonVerbose > 0) printf("worm closed\n");
    if(baryonVerbose > 1) printf("m: %d->0 b[%d][%d]: %d->%d\n", currentSite -> m, lastrho, !lastrhoforward, currentSite -> b[lastrho][!lastrhoforward], currentSite -> b[lastrho][!lastrhoforward]-1);
    #endif
    currentSite -> m = 0;
    currentSite -> b[lastrho][!lastrhoforward] -=1;
    if(currentSite -> b[lastrho][!lastrhoforward] == 0){
      #if DEBUG
      if(baryonVerbose > 1) printf("all baryon loops removed from site -> k[%d][%d]: 0->3 memberOfBaryonLoop: true->false\n", lastrho, !lastrhoforward);
      #endif
      currentSite -> k[lastrho][!lastrhoforward] = 3;
      currentSite -> memberOfBaryonLoop = false;
    }
    return;
  }
  int success = false;
  for(int mu = 0; mu < d && !success; mu++)
  for(int forward = 0; forward < 2; forward++){
    if(currentSite -> k[mu][forward] == 3){
      #if DEBUG
      if(baryonVerbose > 1) printf("found triple dimer in direction %s%d\n", (forward ? "+" : "-"), mu);
      if(baryonVerbose > 2) printf("b[%d][%d]: %d->%d\n b[%d][%d]: %d->-1\n k[%d][%d]: %d->0\n", lastrho, !lastrhoforward, currentSite -> b[lastrho][!lastrhoforward], currentSite -> b[lastrho][!lastrhoforward] - 1, mu, forward, currentSite -> b[mu][forward], mu, forward, currentSite -> k[mu][forward]) ;
      #endif
      currentSite -> b[lastrho][!lastrhoforward] -= 1;
      currentSite -> b[mu][forward] = 1;
      currentSite -> k[mu][forward] = 0;
      currentSite -> memberOfBaryonLoop = true;
      *newmu = mu;
      *newmuforward = forward;
      success = true;
      break;
    }
    if(currentSite -> b[mu][forward] == -1){
      #if DEBUG
      if(baryonVerbose > 1) printf("found baryon loop with b=-1 in direction %s%d", (forward ? "+" : "-"), mu);
      if(baryonVerbose > 2) printf("b[%d][%d]: %d->%d\n b[%d][%d]: %d->0\n", lastrho, !lastrhoforward, currentSite -> b[lastrho][!lastrhoforward], currentSite -> b[lastrho][!lastrhoforward] -1, mu , forward, currentSite -> b[mu][forward]);
      #endif
      currentSite -> b[lastrho][!lastrhoforward] -=1;
      currentSite -> b[mu][forward] = 0;
      if(currentSite -> b[lastrho][!lastrhoforward] == 0){
        #if DEBUG
        if(baryonVerbose > 2) printf("erased all baryon lines from site -> k[%d][%d]: %d->3\n", lastrho, !lastrhoforward, currentSite -> k[lastrho][!lastrhoforward]);
        #endif
        currentSite -> k[lastrho][!lastrhoforward] = 3;
        currentSite -> memberOfBaryonLoop = false;
      }
      *newmu = mu;
      *newmuforward = forward;
      success = true;
      break;
    }
  }

  if(!success){
    fprintf(stderr, "site is in none of the three possible states. Something went wrong\n");
    fprintf(stderr, "k[0][0]=%d k[0][1]=%d k[1][0]=%d k[1][1]=%dk[2][0]=%d k[2][1]=%d k[3][0]=%d k[3][1]=%d\nb[0][0]=%d b[0][1]=%d b[1][0]=%d b[1][1]=%db[2][0]=%d b[2][1]=%d b[3][0]=%d b[3][1]=%d\n m=%d\n", currentSite -> k[0][0], currentSite -> k[0][1], currentSite -> k[1][0], currentSite -> k[1][1], currentSite -> k[2][0], currentSite -> k[2][1], currentSite -> k[3][0], currentSite -> k[3][1], currentSite -> b[0][0], currentSite -> b[0][1], currentSite -> b[1][0], currentSite -> b[1][1], currentSite -> b[2][0], currentSite -> b[2][1], currentSite -> b[3][0], currentSite -> b[3][1], currentSite -> m);
    exit(1);
  }

  int mu = *newmu;
  int forward = *newmuforward;

  int step = forward * 2 - 1;
  if(mu == 0) *t = (*t + step + N_t) % N_t;
  if(mu == 1) *x = (*x + step + N_x) % N_x;
  if(mu == 2) *y = (*y + step + N_y) % N_y;
  if(mu == 3) *z = (*z + step + N_z) % N_z;
  #if DEBUG
  if(baryonVerbose > 0) printf("moving %+d in direction %d\n", step, mu);
  if(mesonVerbose > 0) printf("new (t, x, y, z)=(%d, %d, %d, %d)\n", *t, *x, *y, *z);
  if(mesonVerbose > 0) printf("baryon move 3 complete\n");
  #endif
}


//Karsch and Mütter 1988 table 1
int siteState(struct latticePoint lattice[N_t][N_x][N_y][N_z], int t, int x, int y, int z){
  struct latticePoint currentSite = lattice[t][x][y][z];
  #if DEBUG
  if(currentSite.memberOfBaryonLoop){
    fprintf(stderr, "site is member of bayonloop, so no mesonic states apply\n");
    exit(1);
  }
  #endif
  if(currentSite.n == 3) return 3;
  if(currentSite.n == 2) return 2;
  if(currentSite.n == 1){
    for(int nu = 0; nu < d; nu++)
    for(int forward = 0; forward < 2; forward++){
      if(currentSite.k[nu][forward] == 2) return 4;
      if(currentSite.k[nu][forward] == 1) return 1;
    }
    fprintf(stderr, "site was not in a possible configuration: n = 1, but ks are wrong\n");
    fprintf(stderr, " t, x, n, m, k00, b00, k01, b01, k10, b10, k11, b11,\n");
    fprintf(stderr, "%2d,%2d,", t, x);
    fprintf(stderr, "%2d,", currentSite.n);
    fprintf(stderr, "%2d,", currentSite.m);
    for(int mu = 0; mu < d; mu++)
    for(int forward = 0; forward < 2; forward++){
      fprintf(stderr, "%4d,", currentSite.k[mu][forward]);
      fprintf(stderr, "%4d,", currentSite.b[mu][forward]);
    }
    fprintf(stderr, "\n");
    exit(1);
  }
  // n must equal 0 now
  int oneCounter = 0; // how many links are 1
  for(int nu = 0; nu < d; nu++)
  for(int forward = 0; forward < 2; forward++){
    if(currentSite.k[nu][forward] == 3) return 6;
    oneCounter += currentSite.k[nu][forward] == 1;
    if(oneCounter == 3) return 5;
    if(currentSite.k[nu][forward] == 2) return 0;
  }
  fprintf(stderr, "site was not in a possible configuration\n");
  fprintf(stderr, " t, x, n, m, k00, b00, k01, b01, k10, b10, k11, b11,\n");
  fprintf(stderr, "%2d,%2d,", t, x);
  fprintf(stderr, "%2d,", currentSite.n);
  fprintf(stderr, "%2d,", currentSite.m);
  for(int mu = 0; mu < d; mu++)
  for(int forward = 0; forward < 2; forward++){
    fprintf(stderr, "%4d,", currentSite.k[mu][forward]);
    fprintf(stderr, "%4d,", currentSite.b[mu][forward]);
  }
  fprintf(stderr, "\n");
  exit(1);
}

void MDPUpdate(struct latticePoint lattice[N_t][N_x][N_y][N_z], float gamma, pcg32_random_t* pcg32_state){
  if(gamma != 1){
    fprintf(stderr, "gamma needs to be = 1 for MDPUpdate\n gamma=%g\n", gamma);
    exit(1);
  }
  int tries = 0;
  while(tries < maxTries){
    tries++;
    int t1 = randT(pcg32_state);
    int x1 = randX(pcg32_state);
    int y1 = randY(pcg32_state);
    int z1 = randZ(pcg32_state);
    struct latticePoint* currentSite = &lattice[t1][x1][y1][z1];

    if(currentSite -> memberOfBaryonLoop) continue;

    // TODO implement randDirection function

    int t2 = t1;
    int x2 = x1;
    int y2 = y1;
    int z2 = z1;

    int nu = randInt(0, d - 1, pcg32_state);
    int forward = randInt(0, 1, pcg32_state);

    int step = forward * 2 - 1;
    if(nu == 0) t2 = (t2 + step + N_t) % N_t;
    if(nu == 1) x2 = (x2 + step + N_x) % N_x;
    if(nu == 2) y2 = (y2 + step + N_y) % N_y;
    if(nu == 3) z2 = (z2 + step + N_z) % N_z;

    struct latticePoint* neighborSite = &lattice[t2][x2][y2][z2];

    if(neighborSite -> memberOfBaryonLoop) continue;

    int site1State = siteState(lattice, t1, x1, y1, z1);
    int site2State = siteState(lattice, t2, x2, y2, z2);
    int linkState = currentSite -> k[nu][forward];

    int addMonomer;

    // Karsch and Mütter 1988 table 2


    int lookupIndex = 100 * site1State + 10 * site2State + linkState;
    float weight;

    switch (lookupIndex) {
      case 001:
				weight = 3.0 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 002:
				weight = 4.0 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 101:
				weight = 1.5 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 201:
				weight = 1.0 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 402:
				weight = 2.0 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 501:
				weight = 3.0 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 110:
				weight = 0.3333333333333333 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 210:
				weight = 0.6666666666666666 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 310:
				weight = 1.0 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 221:
				weight = 0.3333333333333333 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 442:
				weight = 0.3333333333333333 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 440:
				weight = 0.3333333333333333 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 111:
				weight = 0.25 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 420:
				weight = 0.6666666666666666 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 430:
				weight = 1.0 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 410:
				weight = 0.3333333333333333 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 551:
				weight = 3.0 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 511:
				weight = 1.5 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 521:
				weight = 1.0 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 330:
				weight = 3.0 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 663:
				weight = 3.0 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
			case 320:
				weight = 2.0 * pow(gamma, 2 * (nu == 0)) * pow(2 * a * mq, -2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = false;
				break;
			case 211:
				weight = 0.5 * pow(gamma, -2 * (nu == 0)) * pow(2 * a * mq, 2);
				if(!(randReal(pcg32_state) < weight)) continue;
				addMonomer = true;
				break;
      default:
        continue;
    }

    if(addMonomer){
      currentSite -> n += 1;
      currentSite -> k[nu][forward] -= 1;
      neighborSite -> n += 1;
      neighborSite -> k[nu][!forward] -= 1;
      return;
    }else{
      currentSite -> n -= 1;
      currentSite -> k[nu][forward] += 1;
      neighborSite -> n -= 1;
      neighborSite -> k[nu][!forward] += 1;
      return;
    }
  }
}

void calculateObservables(struct latticePoint lattice[N_t][N_x][N_y][N_z], int averageOver, float gamma, float mu, float* baryonDensity, float* sign, float* chiralCondensate, float* energyDensity, pcg32_random_t* pcg32_state){
  int baryonAcc = 0;
  int signAcc = 0;
  int signVar;
  for(int i = 0; i < averageOver; i++){
    mesonWorm(lattice, gamma, pcg32_state);
    baryonWorm(lattice, gamma, mu, pcg32_state);
    //MDPUpdate(lattice, gamma, pcg32_state);
    signVar = calcSign(lattice);
    signAcc += signVar;
    baryonAcc += signVar * windingNumbers(lattice);
  }
  *baryonDensity = baryonAcc * 1.0 / signAcc * invV_s;
  if(mq != 0) *chiralCondensate = calcChiralCondensate(lattice, averageOver, gamma, mu, pcg32_state);
  *energyDensity = calcEnergyDensity(lattice, averageOver, gamma, mu, pcg32_state);
  *sign = signAcc * 1.0 / averageOver;
}


float calcEnergyDensity(struct latticePoint lattice[N_t][N_x][N_y][N_z], int averageOver, float gamma, float mu, pcg32_random_t* pcg32_state){
  int links = 0;
  int signVar;
  int signAcc = 0;
  for(int i = 0; i < averageOver; i++){
    mesonWorm(lattice, gamma, pcg32_state);
    baryonWorm(lattice, gamma, mu, pcg32_state);
    signVar = calcSign(lattice);
    signAcc += signVar;
    for(int t = 0; t < N_t; t++)
    for(int x = 0; x < N_x; x++)
    for(int y = 0; y < N_y; y++)
    for(int z = 0; z < N_z; z++){
      struct latticePoint* currentSite = &lattice[t][x][y][z];
      links += 2 * currentSite -> k[0][1] * signVar;
      links += 3 * abs(currentSite -> b[0][1]) * signVar;
    }
  }
  // TODO why /-3??
  float energyDensity = -invV_s / N_t / gamma * links / signAcc / -3;
  return energyDensity;
}

int monomerNumbers(struct latticePoint lattice[N_t][N_x][N_y][N_z]){
  int monomers = 0;
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++)
  for(int y = 0; y < N_y; y++)
  for(int z = 0; z < N_z; z++){
    monomers += lattice[t][x][y][z].n;
  }
  return monomers;
}

float calcChiralCondensate(struct latticePoint lattice[N_t][N_x][N_y][N_z], int averageOver, float gamma, float mu, pcg32_random_t* pcg32_state){
  int t0;
  int x0;
  int y0;
  int z0;

  int t;
  int x;
  int y;
  int z;

  int lastnu;
  int lastforward;

  int lastrho;
  int lastrhoforward;

  float observable = 0; //corresponds to O(psibar psi)
  int signVar;
  int signAcc = 0;
  int V = N_t * N_x * N_y * N_z;


  for(int i = 0; i < averageOver; i++){

    baryonWorm(lattice, gamma, mu, pcg32_state);
    mesonWorm(lattice, gamma, pcg32_state);
    //for(int j = 0; j < 10; j++) MDPUpdate(lattice, gamma, pcg32_state);

    int V_D = V - countBaryonicSites(lattice); // number of meson sites

    signVar = calcSign(lattice);
    signAcc += signVar;
    int step1return = mesonWormStep1(lattice, &t0, &x0, &y0, &z0, &t, &x, &y, &z, &lastnu, &lastforward, pcg32_state);
    int step3return;
    if(step1return != 0){
      continue;
    }


    do {

      observable += signVar / mesonWormStep2W_D(lattice, t, x, y, z, gamma) * V_D / V;

      mesonWormStep2(lattice, &t, &x, &y, &z, lastnu, lastforward, &lastrho, &lastrhoforward, gamma, pcg32_state);

      step3return = mesonWormStep3(lattice, &t, &x, &y, &z, lastrho, lastrhoforward, &lastnu, &lastforward, pcg32_state);


    } while(step3return != 0);

  }

  return 4 * a * paramN * mq * observable / signAcc;

}

int windingNumbers(struct latticePoint lattice[N_t][N_x][N_y][N_z]){
  int result = 0;
  int removed[N_t][N_x][N_y][N_z] = {};
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++)
  for(int y = 0; y < N_y; y++)
  for(int z = 0; z < N_z; z++){
    if(!removed[t][x][y][z] && lattice[t][x][y][z].memberOfBaryonLoop){
      removed[t][x][y][z] = true;
      int tWalker = t;
      int xWalker = x;
      int yWalker = y;
      int zWalker = z;
      struct latticePoint* currentSite = &lattice[tWalker][xWalker][yWalker][zWalker];
      do{
        int loopComplete = false;
        for(int mu = 0; mu < d && !loopComplete; mu++)
        for(int forward = 0; forward < 2 && !loopComplete; forward++){
          if(currentSite -> b[mu][forward] == -1){
            int step = forward * 2 - 1;
            //cout << "moving " << step << " in direction " << mu << "\n";
            if(mu == 0 && (tWalker + step == N_t)) result -= 1;
            if(mu == 0 && (tWalker + step == -1)) result += 1;
            if(mu == 0) tWalker = (tWalker + step + N_t) % N_t;
            if(mu == 1) xWalker = (xWalker + step + N_x) % N_x;
            if(mu == 2) yWalker = (yWalker + step + N_y) % N_y;
            if(mu == 3) zWalker = (zWalker + step + N_z) % N_z;
            //cout << "new (t,x,y,z)=(" << tWalker << "," << xWalker << "," << yWalker << "," << zWalker << ")" << endl;
            //cout << "start (t,x,y,z)=(" << t << "," << x << "," << y << "," << z << ")" << endl;
            removed[tWalker][xWalker][yWalker][zWalker] = true;
            if(t == tWalker && x == xWalker && y == yWalker && z == zWalker) loopComplete = true;
            currentSite = &lattice[tWalker][xWalker][yWalker][zWalker];
          }
        }
      }while(t != tWalker || x != xWalker || y != yWalker || z != zWalker);
      //cout << "loop completed\n";
    }
  }
  #if DEBUG
  if(windingDebug) printLatticeState(lattice);
  if(windingDebug) printf("%d\n", result);
  #endif
  return result;
}

int calcSign(struct latticePoint lattice[N_t][N_x][N_y][N_z]){
  // right below (4.13) with eta from below (2.22)
  int signVar = 1;
  int removed[N_t][N_x][N_y][N_z] = {};
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++)
  for(int y = 0; y < N_y; y++)
  for(int z = 0; z < N_z; z++){
    if(!removed[t][x][y][z] && lattice[t][x][y][z].memberOfBaryonLoop){
      signVar *= -1;
      removed[t][x][y][z] = true;
      int tWalker = t;
      int xWalker = x;
      int yWalker = y;
      int zWalker = z;
      struct latticePoint* currentSite = &lattice[tWalker][xWalker][yWalker][zWalker];
      do{
        int loopComplete = false;
        for(int mu = 0; mu < d && !loopComplete; mu++)
        for(int forward = 0; forward < 2 && !loopComplete; forward++){
          if(currentSite -> b[mu][forward] == -1){
            int step = forward * 2 - 1;
            if(mu == 0 && (tWalker + step == N_t || tWalker + step == -1)) signVar *= -1;
            if(mu == 0) tWalker = (tWalker + step + N_t) % N_t;
            if(mu == 1) xWalker = (xWalker + step + N_x) % N_x;
            if(mu == 2) yWalker = (yWalker + step + N_y) % N_y;
            if(mu == 3) zWalker = (zWalker + step + N_z) % N_z;
            int eta = 1;
            if((mu > 0)) eta *= pow(-1, tWalker);
            if((mu > 1)) eta *= pow(-1, xWalker);
            if((mu > 2)) eta *= pow(-1, yWalker);
            signVar *= eta;
            signVar *= step;
            removed[tWalker][xWalker][yWalker][zWalker] = true;
            loopComplete = (t == tWalker && x == xWalker && y == yWalker && z == zWalker);
            currentSite = &lattice[tWalker][xWalker][yWalker][zWalker];
          }
        }
      }while(t != tWalker || x != xWalker || y != yWalker || z != zWalker);
    }
  }
  #if DEBUG
  if(signDebug) printLatticeState(lattice);
  if(signDebug) printf("%d\n", signVar);
  #endif
  return signVar;
}

void initLattice(struct latticePoint lattice[N_t][N_x][N_y][N_z]){
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++)
  for(int y = 0; y < N_y; y++)
  for(int z = 0; z < N_z; z++){
    struct latticePoint* currentSite = &lattice[t][x][y][z];
    if(parity(t, x, y, z) == 1){
      currentSite -> k[0][0] = 1;
      currentSite -> k[0][1] = 2;
    }else{
      currentSite -> k[0][0] = 2;
      currentSite -> k[0][1] = 1;
    }
  }
  for(int t = 0; t < N_t; t++){
    struct latticePoint* currentSite = &lattice[t][0][0][0];
    currentSite -> k[0][0] = 0;
    currentSite -> k[0][1] = 0;
    currentSite -> b[0][0] = -1;
    currentSite -> b[0][1] = 1;
    currentSite -> memberOfBaryonLoop = true;
  }
}

void validityCheck(struct latticePoint lattice[N_t][N_x][N_y][N_z]){
  for(int t = 0; t < N_t ; t++)
  for(int x = 0; x < N_x ; x++)
  for(int y = 0; y < N_y ; y++)
  for(int z = 0; z < N_z ; z++){
    struct latticePoint* currentSite = &lattice[t][x][y][z];

    int realMember = false;
    for(int nu = 0; nu < d; nu++)
    for(int forward = 0; forward < 2; forward++){
      if(currentSite -> b[nu][forward] != 0 && !currentSite -> memberOfBaryonLoop){
        fprintf(stderr, "at site (t, x, y, z)=(%d, %d, %d, %d) condition for memberOfBaryonLoop violated. Namely b[%d][%d]=%d, but memberOfBaryonLoop=false\n", t, x, y ,z, nu, forward, currentSite -> b[nu][forward]);
        exit(1);
      }
      if(currentSite -> b[nu][forward] !=0) realMember = true;
    }
    if(realMember != currentSite -> memberOfBaryonLoop){
      fprintf(stderr, "at site (t, x, y, z)=(%d, %d, %d, %d) condition for memberOfBaryonLoop violated. Namely all b are 0, but memberOfBaryonLoop=true\n", t, x, y, z);
      exit(1);
    }

    // calculate N -- Fromm 2010 eq. (4.11)
    int N = currentSite -> n;
    for(int nu = 0; nu < d; nu++)
    for(int forward = 0; forward < 2; forward++){
      N += currentSite -> k[nu][forward];
    }

    if(N % 3 != 0){
      fprintf(stderr, "at site (t, x, y, z)=(%d, %d, %d, %d) condition for N violated. Namely N=%d", t, x, y, z, N);
      exit(1);
    }

    for(int nu = 0; nu < d; nu++)
    for(int forward = 0; forward < 2; forward++){
      int neighbort = t;
      int neighborx = x;
      int neighbory = y;
      int neighborz = z;
      int step = forward * 2 - 1;
      if(nu == 0) neighbort = (neighbort + step + N_t) % N_t;
      if(nu == 1) neighborx = (neighborx + step + N_x) % N_x;
      if(nu == 2) neighbory = (neighbory + step + N_y) % N_y;
      if(nu == 3) neighborz = (neighborz + step + N_z) % N_z;

      struct latticePoint* neighbor = &lattice[neighbort][neighborx][neighbory][neighborz];
      if(currentSite -> k[nu][forward] != neighbor -> k[nu][!forward]){
        fprintf(stderr, "at site (t, x, y, z)=(%d, %d, %d, %d) neighbor condition violated. Namely direction %+d%d k=%d neighbor k=%d\n", t, x, y, z, step, nu, currentSite -> k[nu][forward], neighbor -> k[nu][!forward]);
        exit(1);
      }
    }
  }
}

int countBaryonicSites(struct latticePoint lattice[N_t][N_x][N_y][N_z]){
  int count = 0;
  for(int t = 0; t < N_t ; t++)
  for(int x = 0; x < N_x ; x++)
  for(int y = 0; y < N_y ; y++)
  for(int z = 0; z < N_z ; z++){
    struct latticePoint* currentSite = &lattice[t][x][y][z];
    if(currentSite -> memberOfBaryonLoop) count++;
  }
  return count;
}

float randReal(pcg32_random_t* pcg32_state){
  const float S = (1.0f - probabilityEpsilon) / UINT32_MAX;
  return pcg32_random_r(pcg32_state) * S;
}

unsigned int randT(pcg32_random_t* pcg32_state){ // these are compiled to bitwise operations and are thus a lot faster than the randInt function, which uses idiv
  return pcg32_random_r(pcg32_state) % N_t;
}

unsigned int randX(pcg32_random_t* pcg32_state){ // these are compiled to bitwise operations and are thus a lot faster than the randInt function, which uses idiv
  return pcg32_random_r(pcg32_state) % N_x;
}

unsigned int randY(pcg32_random_t* pcg32_state){ // these are compiled to bitwise operations and are thus a lot faster than the randInt function, which uses idiv
  return pcg32_random_r(pcg32_state) % N_y;
}

unsigned int randZ(pcg32_random_t* pcg32_state){ // these are compiled to bitwise operations and are thus a lot faster than the randInt function, which uses idiv
  return pcg32_random_r(pcg32_state) % N_z;
}

unsigned int randInt(int a, int b, pcg32_random_t* pcg32_state){
  return pcg32_random_r(pcg32_state) % (b - a + 1) + a;
}

int parity(int t, int x, int y, int z){
  return ((t + x + y + z) % 2) * 2 - 1;
}

void printLatticeState(struct latticePoint lattice[N_t][N_x][N_y][N_z]){
  if(d != 2  || N_y != 1 || N_z != 1){
    fprintf(stderr, "complete output for lattice states only in 1 + 1 dimensions\n");
    exit(1);
  }
  printf(" t, x, n, m, k00, b00, k01, b01, k10, b10, k11, b11,\n");
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    struct latticePoint* currentSite = & lattice[t][x][0][0];
    printf("%2d,%2d,", t, x);
    printf("%2d,", currentSite -> n);
    printf("%2d,", currentSite -> m);
    for(int mu = 0; mu < d; mu++)
    for(int forward = 0; forward < 2; forward++){
      printf("%4d,", currentSite -> k[mu][forward]);
      printf("%4d,", currentSite -> b[mu][forward]);
    }
    printf("\n");
  }
}

void thermalize(struct latticePoint lattice[N_t][N_x][N_y][N_z], int iterations, float gamma, float mu, char* status, pcg32_random_t* pcg32_state){
  struct winsize w;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
  int terminalWidth = w.ws_col;
  float fracDone;
  int textLength;
  int barLength;
  int filledLength;
  int remainderLength;
  char* outString = (char*) malloc(sizeof(int) * 500);
  float invIterations = 1.0 / iterations;
  for(int i = 0; i < iterations; i++){
    sprintf(outString, "thermalizing: %d of %d iterations [", i, iterations);
    textLength = strlen(outString);
    barLength = (terminalWidth - (textLength + 1));
    if(barLength < 0){
      fprintf(stderr, "too much text to put in nextOut - Aborting!\n");
      exit(1);
    }
    char* bar = (char*) malloc(sizeof(char) * (barLength + 2));
    fracDone = i * invIterations;
    filledLength = ceil(barLength * fracDone);
    remainderLength = barLength - filledLength;
    for(int j = 0; j < filledLength; j++){
      bar[j] = '#';
    }
    for(int j = 0; j < remainderLength; j++){
      bar[j + filledLength] = '-';
    }
    bar[barLength] = ']';
    bar[barLength + 1] = 0;
    sprintf(status,"%s%s", outString, bar);
    free(bar);

    mesonWorm(lattice, gamma, pcg32_state);
    baryonWorm(lattice, gamma, mu, pcg32_state);
    //for(int j = 0; j < 10; j++) MDPUpdate(lattice, gamma, pcg32_state);
  }
  validityCheck(lattice);
  free(outString);
}

void *printGammaRun(void* params){

  struct gammaRunParameters* realParams = params;

  float gamma = realParams -> gamma;
  float startingMu = realParams -> startingMu;
  float endingMu = realParams -> endingMu;
  float muStep = realParams -> muStep;
  int averageOver = realParams -> averageOver;
  int NErrorSamples = realParams -> NErrorSamples;
  char* filename = realParams -> filename;
  int dynamicalParameters = realParams -> dynamicalParameters;
  float maxError = realParams -> maxError;
  float desireddn = realParams -> desireddn;
  float deltadn = realParams -> deltadn;
  float maxMuStep = realParams -> maxMuStep;
  float minMuStep = realParams -> minMuStep;
  int maxAverageOver = realParams -> maxAverageOver;
  char* lastOutput = realParams -> lastOutput;
  char* status = realParams -> status;
  int* terminalWidth = realParams -> terminalWidth;
  int* done = realParams -> done;


  // initialize RNG
  pcg32_random_t pcg32_state;

  pcg32_srandom_r(&pcg32_state, time(NULL), 26);

  *done = false;

  FILE* file;
  if(writeFlag){
    printf("\t\t\tsaving in filename %s\n", filename);
    file = fopen(filename, "w+");
  }
  if(writeFlag) fprintf(file, "mu, gamma, mq, baryonDensityAverage, baryonDensityError, baryonAutocorr, signAverage, signError, signAutocorr, chiralCondensateAverage, chiralCondensateError, chiralCondensateAutocorr, energyDensityAverage, energyDensityError, energyDensityAutocorr, samples, muStep\n");
  char* nextOut = malloc(sizeof(char) * *terminalWidth);
  float newBaryonDensitySample;
  float newChiralCondensateSample;
  float newEnergyDensitySample;

  int textLength;
  int remainderLength;
  int filledLength;
  int barLength;
  float fracDone;
  float invErrorSamples = 1.0 / NErrorSamples;

  int initialAverageOver = averageOver;

  float lastBaryonAvg = 0;
  struct latticePoint lattice[N_t][N_x][N_y][N_z] = {};
  initLattice(lattice);
  validityCheck(lattice);
  for(float mu = startingMu; mu <= endingMu; mu += muStep){

    //char* thermalizeStatus = (char*) malloc(sizeof(char) * *terminalWidth);
    thermalize(lattice, thermalisationIterations, gamma, mu, status, &pcg32_state);
    //free(thermalizeStatus);
    validityCheck(lattice);
    float *baryonDensitySamples = (float *) malloc(sizeof(float) * NErrorSamples);
    float *signSamples = (float*) malloc(sizeof(float) * NErrorSamples);
    float *chiralCondensateSamples = (float*) malloc(sizeof(float) * NErrorSamples);
    float *energyDensitySamples = (float*) malloc(sizeof(float) * NErrorSamples);
    for(int i = 0; i < NErrorSamples; i++){
      //printf("%d\n", i);
      fracDone = i * invErrorSamples;
      sprintf(nextOut, "gamma=%-5.3g mu=%-7.5g dmu=%-7.5g averaging over %d configs %3g%% done [", gamma, mu, muStep, averageOver, round(100 * fracDone));

      textLength = strlen(nextOut);
      barLength = (*terminalWidth - (textLength + 1));
      if(barLength < 0){
        fprintf(stderr, "too much text to put in nextOut - Aborting!\n");
        exit(1);
      }
      char* bar = (char*) malloc(sizeof(char) * (barLength + 2));
      filledLength = ceil(barLength * fracDone);
      remainderLength = barLength - filledLength;
      for(int j = 0; j < filledLength; j++){
        bar[j] = '#';
      }
      for(int j = 0; j < remainderLength; j++){
        bar[j + filledLength] = '-';
      }
      bar[barLength] = ']';
      bar[barLength + 1] = 0;
      sprintf(status,"%s%s", nextOut, bar);
      free(bar);
      //printf("%s\n", status);
      calculateObservables(lattice, averageOver, gamma, mu, &newBaryonDensitySample, &signSamples[i], &newChiralCondensateSample, &newEnergyDensitySample, &pcg32_state);

      if(isnan(newBaryonDensitySample) || isinf(newBaryonDensitySample) || isnan(newChiralCondensateSample) || isinf(newChiralCondensateSample) || isnan(newEnergyDensitySample) || isinf(newEnergyDensitySample)){
        i--;
        continue;
      }
      baryonDensitySamples[i] = newBaryonDensitySample;
      chiralCondensateSamples[i] = newChiralCondensateSample;
      energyDensitySamples[i] = newEnergyDensitySample;
    }
    float baryonDensityAverage;
    float baryonDensityError;
    float baryonAutocorr;
    jackknife(baryonDensitySamples, NErrorSamples, &baryonDensityAverage, &baryonDensityError, &baryonAutocorr);

    float signAverage;
    float signError;
    float signAutocorr;
    jackknife(signSamples, NErrorSamples, &signAverage, &signError, &signAutocorr);

    // adjust sample size
    averageOver = initialAverageOver / signAverage;

    float chiralCondensateAverage;
    float chiralCondensateError;
    float chiralCondensateAutocorr;
    jackknife(chiralCondensateSamples, NErrorSamples, & chiralCondensateAverage, &chiralCondensateError, &chiralCondensateAutocorr);

    float energyDensityAverage;
    float energyDensityError;
    float energyDensityAutocorr;
    jackknife(energyDensitySamples, NErrorSamples, & energyDensityAverage, &energyDensityError, &energyDensityAutocorr);


    free(baryonDensitySamples);
    free(signSamples);
    free(chiralCondensateSamples);
    free(energyDensitySamples);

    if(dynamicalParameters && fabs(baryonDensityAverage - lastBaryonAvg) > desireddn + deltadn && muStep != minMuStep){
      mu -= muStep;
      muStep /= 2;
      if(muStep < minMuStep){
        muStep = minMuStep;
      }
      continue;
    }
    if(dynamicalParameters && (baryonDensityError > maxError || isnan(baryonDensityError)) && (maxAverageOver == -1 || averageOver != maxAverageOver)){
      averageOver *= 2;
      if(averageOver < maxAverageOver || maxAverageOver == -1){
        mu -= muStep;
        continue;
      }
      averageOver = maxAverageOver;
    }else if(dynamicalParameters && baryonDensityError < maxError * defaultMaxErrorDecFrac){
      averageOver /=2;
      averageOver = fmax(initialAverageOver, averageOver);
    }
    if(dynamicalParameters && fabs(baryonDensityAverage - lastBaryonAvg) < desireddn - deltadn){
      muStep *= 2;
    }
    muStep = fmin(maxMuStep, fmax(minMuStep , muStep));
    lastBaryonAvg = baryonDensityAverage;
    if(writeFlag) fprintf(file, "%g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %d, %g\n", mu, gamma, mq, baryonDensityAverage, baryonDensityError, baryonAutocorr, signAverage, signError, signAutocorr, chiralCondensateAverage, chiralCondensateError, chiralCondensateAutocorr, energyDensityAverage, energyDensityError, energyDensityAutocorr, averageOver * NErrorSamples, muStep);
    if(writeFlag) fflush(file);
    sprintf(lastOutput, "gamma=%-5.3g mu=%-7.5g n=%-12g nerr=%-12g autocorrelation time=%-12g average sign=%-12g", gamma, mu, baryonDensityAverage, baryonDensityError, baryonAutocorr, signAverage);
  }
  free(nextOut);
  *done = true;
}

void printObservables(float startingGamma, float endingGamma, float gammaStep, float startingMu, float endingMu, float muStep, int averageOver, int NErrorSamples, char* filename, int dynamicalParameters, float maxError, float desireddn, float deltadn, float maxMuStep, float minMuStep, int maxAverageOver, int threadCount){
  if(writeFlag){
    char* prefix = (char*) malloc(sizeof(char) * 50);
    sprintf(prefix, "%dx%dx%dx%d_%s", N_t, N_x, N_y, N_z, filename);
    strcpy(filename, prefix);
    free(prefix);
    printf("saving in filename %s\n", filename);
    //exit(1);
    //file = fopen(filename, "w+");
    //fprintf(file, "[\n");
  }else{
    printf("\nnote: no output is written to file\n\n");
  }
  printf("grid dimensions: %dx%dx%dx%d\n", N_t, N_x, N_y, N_z);
  printf("d=%d\n\n", d);
  /*int VParameterSpace = 0;

  // I don't have a better way to calculate this...
  // straightforewardly calculating always breaks because of floating point edgecases
  for(float g = startingGamma; g <= endingGamma; g+=gammaStep){
    for(float m = startingMu; m <= endingMu; m+=muStep){
      VParameterSpace += 1;
    }
  }
  int totalSamples = NErrorSamples * VParameterSpace;
  int processedSamples = 0;
*/


  struct winsize w;

  int gammaCount = 0;

  for(float g = startingGamma; g <= endingGamma; g+=gammaStep){
    gammaCount++;
  }

  float* gammaList = (float*) malloc(sizeof(float) * gammaCount);
  int gammaIndex = 0;

  for(float g = startingGamma; g < endingGamma; g += gammaStep){
    gammaList[gammaIndex] = g;
    gammaIndex++;
  }
  gammaIndex = 0;


  pthread_t* threadIDs = (pthread_t*) malloc(sizeof(pthread_t) * threadCount);
  int* doneList = (int*) malloc(sizeof(int) * threadCount);
  for(int i = 0; i < threadCount; i++){
    doneList[i] = true;
  }
  int threadIndex = 0;


  struct gammaRunParameters params;

  int threadOutputMaxSize = 500;
  char* threadOutputs = calloc(threadOutputMaxSize * threadCount, sizeof(char));

  int threadStatusMaxSize = 500;
  char* threadStatuses = calloc(threadStatusMaxSize * threadCount, sizeof(char));

  int maxFilenameSize = 200;
  char* threadFilenames = malloc(sizeof(char) * maxFilenameSize * threadCount);

  char* folderedFilename = (char*) malloc(sizeof(char) * maxFilenameSize);
  sprintf(folderedFilename, "out/%s", filename);
  mkdir(folderedFilename, 0777);
    //printf("folder creation failed\n");
    //exit(1);

  int terminalWidth;

  printf("\e[?25l");
  printf("\33[2J"); // clear screen
  while(gammaIndex < gammaCount){
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    terminalWidth = w.ws_col;
    //printf("\33[2J"); // clear screen
    printf("\33[2K"); // clear line
    printf("\33[%d;0H", 5 * threadIndex + 1); // set cursor Position
    if(doneList[threadIndex]){
      sprintf(&threadFilenames[maxFilenameSize * threadIndex], "out/%s/%s_%g.csv", filename, filename, gammaList[gammaIndex]);
      params.gamma = gammaList[gammaIndex];
      params.startingMu = startingMu;
      params.endingMu = endingMu;
      params.muStep = muStep;
      params.averageOver = averageOver;
      params.NErrorSamples = NErrorSamples;
      params.filename = &threadFilenames[maxFilenameSize * threadIndex];
      params.dynamicalParameters = dynamicalParameters;
      params.maxError = maxError;
      params.desireddn = desireddn;
      params.deltadn = deltadn;
      params.maxMuStep = maxMuStep;
      params.minMuStep = minMuStep;
      params.maxAverageOver = maxAverageOver;
      params.lastOutput = &threadOutputs[threadOutputMaxSize * threadIndex];
      params.status = &threadStatuses[threadOutputMaxSize * threadIndex];
      params.terminalWidth = &terminalWidth;
      params.done = &doneList[threadIndex];
      pthread_create(&threadIDs[threadIndex], NULL, printGammaRun, &params);
      gammaIndex++;
    }else{
      printf("\33[2K"); // clear line
      printf("thread %d", threadIndex);
      if(writeFlag){
        printf(" saving results in %s\n", &threadFilenames[maxFilenameSize * threadIndex]);
      }else{
        printf("\n");
      }
      printf("\33[2K"); // clear line
      printf("%s\n", &threadStatuses[threadStatusMaxSize * threadIndex]);
      printf("\33[2K"); // clear line
      printf("%s\n", &threadOutputs[threadOutputMaxSize * threadIndex]);
    }
    threadIndex++;
    threadIndex = threadIndex % threadCount;
    msleep(10);
  }
  int alldone = true;
  printf("\33[2J"); // clear screen
  do
  {
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    terminalWidth = w.ws_col;
    alldone = true;
    for(int threadIndex = 0; threadIndex < threadCount; threadIndex++){
      if(!doneList[threadIndex]) alldone = false;
      printf("\33[%d;0H", 5 * threadIndex + 1); // set cursor Position
      printf("thread %d\n", threadIndex);
      printf("%s\n", &threadStatuses[threadStatusMaxSize * threadIndex]);
      printf("%s\n", &threadOutputs[threadOutputMaxSize * threadIndex]);
      if(doneList[threadIndex]) printf("done!\n");
    }
    msleep(10);
  }while(!alldone);

  printf("\33[2J");
  free(threadFilenames);
  free(threadOutputs);
  free(threadIDs);
  free(doneList);
  free(gammaList);
  printf("\e[?25h");
}

void jackknife(float values[], int length, float* avg, float* err, float* autocorr){ // https://en.wikipedia.org/wiki/Jackknife_resampling
  float average = 0;
  for(int i = 0; i < length; i++){
    average += values[i];
  }
  average /= length;

  *autocorr = autocorrelation(values, length, average);
  if(isnan(*autocorr)) *autocorr = 1;

  float jackvar = 0;
  for(int i = 0; i < length; i++){
    jackvar += pow(average - values[i], 2) / (length * (length - 1));
  }
  *err = sqrt(jackvar);
  *avg = average;
}

float autocorrelation(float values[], int length, float average){ // compare Jacques Bloch SS 2021 p,35ff,
  float result = 0.5;
  float invLength = 1.0 / length;
  float* covs = malloc(length * sizeof(float));
  for(int t = 0; t < length; t++){
    for(int i = 0; i < length - t; i++){
      covs[t] += (values[i] - average) * (values[i + t] - average) / length;
    }
  }
  for(int t = 1; t < length; t++){
    float summand = (1.0 - t * invLength) * covs[t] / covs[0];
    if(summand < 0) break;
    result += summand;
  }
  free(covs);
  return result;
}

void statisticalAnalysis(float values[], int length, float* avg, float* err){
  float average = 0;
  for(int i = 0; i < length; i++){
    average += values[i];
  }
  average = average / length;
  *avg = average;
  float error = 0;
  for(int i = 0; i < length; i++){
    error += pow(values[i] - average, 2);
  }
  *err = sqrt(error / length);
}

/* msleep(): Sleep for the requested number of milliseconds. */
int msleep(long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

void pcg32_srandom_r(pcg32_random_t* rng, uint64_t initstate, uint64_t initseq)
{
    rng->state = 0U;
    rng->inc = (initseq << 1u) | 1u;
    pcg32_random_r(rng);
    rng->state += initstate;
    pcg32_random_r(rng);
}


uint32_t pcg32_random_r(pcg32_random_t* rng)
{
    uint64_t oldstate = rng->state;
    rng->state = oldstate * 6364136223846793005ULL + rng->inc;
    uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
    uint32_t rot = oldstate >> 59u;
    return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}
