final int d = 2;
final int maxLoadSteps = 400000;
int scrubbingSteps = 1;
boolean playing = false;

final String mode = "winding"; // worm or winding

final PVector borders = new PVector(150, 150);
final float siteSize = 10;
final float mesonLineSpacing = 5;
final float baryonArrowSize = 5;
final boolean recording = false;
PVector spacing;
int startingStep = 00000;
int currentStep = 0;
int[][] wormHead;
int[] windingNumbers;

final color mesonColor = color(255,0,0);
final color baryonColor = color(0,255,0);
final color WormHeadColor = color(0,0,255);

latticePoint[][][] lattice;
int N_t;
int N_x;

class latticePoint{
  int t;
  int x;
  int n;
  int m;
  int[][] k = new int[d][2];
  int[][] b = new int[d][2];
  latticePoint(int[] args){
    t = args[0];
    x = args[1];
    n = args[2];
    m = args[3];
    for(int mu = 0; mu < d; mu++)
    for(int forward = 0; forward < 2; forward++){
      k[mu][forward] = args[4 + (mu * 2 + forward) * 2];
      b[mu][forward] = args[5 + (mu * 2 + forward) * 2];
    }
  }

  void show(){
    float[] pixelCoords = convertToPixels(x, t);
    fill(255);
    if(n == 1) fill(mesonColor);
    if(m == 1) fill(baryonColor);
    if(mode == "worm") if(x == wormHead[currentStep][1] && t == wormHead[currentStep][0]) fill(WormHeadColor);
    //println(wormHead[currentStep]);
    //exit();
    ellipse(pixelCoords[0], pixelCoords[1], siteSize, siteSize);
    for(int mu = 0; mu < d; mu++)
    for(int forward = 0; forward < 2; forward++){
      drawMesonLine(pixelCoords[0], pixelCoords[1], mu, forward, k[mu][forward]);
      drawBaryonLine(pixelCoords[0], pixelCoords[1], mu, forward, b[mu][forward]);
    }
  }
}

void setup(){
  if(recording) playing = true;
  size(700,700);
  background(255);
  println("loading File");
  String[] lines = loadStrings("states");
  println("File loaded. " + str(lines.length) + " lines");
  String[] metaArgs = lines[0].split(",");
  N_t = int(metaArgs[0]);
  N_x = int(metaArgs[1]);
  spacing = new PVector((width - 2 * borders.x) / (N_x - 1), (height - 2 * borders.y) / (N_t - 1));
  int steps = (lines.length - 1) / (N_t * N_x + 1);
  println(str(steps) + " steps found");
  if(steps > maxLoadSteps){
    println(str(steps) + " steps found => only loading " + str(maxLoadSteps));
    steps = maxLoadSteps;
  }
  lattice = new latticePoint[steps][N_t][N_x];
  if(mode == "worm") wormHead = new int[steps][2];
  if(mode == "winding") windingNumbers = new int[steps];
  for(int i = 0; i <steps; i++){
    if((i + 1) % 5000 == 0) println("reading step " + str(i + 1) + " of " + str(steps));
    if(mode == "worm") wormHead[i][0] = int(lines[1 + (i + startingStep) * (N_x * N_t + 1)].split(",")[3]);
    if(mode == "worm") wormHead[i][1] = int(lines[1 + (i + startingStep) * (N_x * N_t + 1)].split(",")[4]);
    if(mode == "winding") windingNumbers[i] = int(lines[(i + 1 + startingStep) * (N_x * N_t + 1)]);
    for(int t = 0; t < N_t; t++)
    for(int x = 0; x < N_x; x++){
      if(mode == "worm") lattice[i][t][x] = readLine(lines[x + N_x * (t + N_t * (i + startingStep)) + 2 + (i + startingStep)]);
      if(mode == "winding") lattice[i][t][x] = readLine(lines[x + N_x * (t + N_t * (i + startingStep)) + 1 + (i + startingStep)]);
    }
  }
}

void draw(){
  background(255);
  if(playing) currentStep++;
  if(currentStep == lattice.length - 1){
    if(recording) exit();
    playing = false;
  }
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[currentStep][t][x].show();
  }
  if(recording) saveFrame("out/step" + nf(currentStep + startingStep, 6) + ".png");
  stroke(0);
  fill(0);
  if(mode == "winding") text(windingNumbers[currentStep], 50, 100);
  text(nf(currentStep + startingStep, 6), 50, 50);
}

latticePoint readLine(String line){
  String[] arguments = line.split(",");
  int[] argInts = new int[arguments.length];
  //println(arguments);
  for (int i = 0; i < arguments.length ; i++) {
    argInts[i] = int(arguments[i]);
  }
  return new latticePoint(argInts);
}

float[] convertToPixels(float x, float y){
  float[] pixels = new float[2];
  pixels[0] = map(x, 0, N_x - 1, borders.x, width - borders.x);
  pixels[1] = map(y, N_t - 1, 0, borders.y, height - borders.y);
  return pixels;
}

void drawMesonLine(float x, float y, int nu, int forward, int k){
  if(k == 0) return;
  float direction = PI * forward + PI/2 * nu;
  float offset = (k - 1) * 0.5 * mesonLineSpacing;
  for(int i = 0; i < k; i++){
    float startX = x - sin(direction) * siteSize + cos(direction) * (mesonLineSpacing * i - offset);
    float startY = y + cos(direction) * siteSize + sin(direction) * (mesonLineSpacing * i - offset);
    float endX = x - sin(direction) * spacing.x/2 + cos(direction) * (mesonLineSpacing * i - offset);
    float endY = y + cos(direction) * spacing.y/2 + sin(direction) * (mesonLineSpacing * i - offset);
    line(startX, startY, endX, endY);
  }
}

void drawBaryonLine(float x, float y, int nu, int forward, int b){
  if(b == 0) return;
  //if(b == 1) return;
  float direction = PI * forward + PI/2 * nu;
  float startX = x - sin(direction) * siteSize;
  float startY = y + cos(direction) * siteSize;
  float endX = x - sin(direction) * spacing.x/2;
  float endY = y + cos(direction) * spacing.y/2;
  line(startX, startY, endX, endY);
  direction += PI/2;
  PVector midpoint = new PVector(map(0.5, 0, 1, startX, endX), map(0.5, 0, 1, startY, endY));
  PVector point1 = new PVector(midpoint.x - (sin(direction) + cos(direction) * b) * baryonArrowSize, midpoint.y + (cos(direction) - sin(direction) * b) * baryonArrowSize) ;
  PVector point2 = new PVector(midpoint.x - (-sin(direction) + cos(direction) * b) * baryonArrowSize, midpoint.y + (-cos(direction) - sin(direction) * b) * baryonArrowSize) ;
  PVector point3 = new PVector(midpoint.x + (cos(direction) * b) * baryonArrowSize, midpoint.y + sin(direction) * b * baryonArrowSize) ;
  fill(0);
  triangle(point1.x, point1.y, point2.x, point2.y, point3.x, point3.y);
}

void keyPressed(){
  if(recording) return;
  if(key == ' ') playing = !playing;
  if(key == CODED){
    if(keyCode == LEFT){
      currentStep-= scrubbingSteps;
      currentStep = max(0, currentStep);
    }
    if(keyCode == RIGHT){
      currentStep+= scrubbingSteps;
      currentStep = min(currentStep, lattice.length-1);
    }
    if(keyCode == UP){
      scrubbingSteps *= 10;
      println("now scrubbing " + str(scrubbingSteps) + " steps");
    }
    if(keyCode == DOWN){
      scrubbingSteps /= 10;
      scrubbingSteps = max(1, scrubbingSteps);
      println("now scrubbing " + str(scrubbingSteps) + " steps");
    }
  }
}
